#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>

using namespace std;

int main(){
	//INPUT
	vector<float> resultados; //Aqui se almacena los resultados
	int triangulosCantidad;
	cin >> triangulosCantidad;//pedir la cantidad de triangulos

	for(int i = 0; i < triangulosCantidad; i++){
		int ladoTriangulo;



		cin >> ladoTriangulo;//pedir el valor de los lados del triangulo
					//equilatero

		resultados.push_back((sqrt(3)/2)*ladoTriangulo);
	}

	//IMPRIMIR RESULTADOS


	for(int i = 0; i < triangulosCantidad; i++){
		cout << fixed << setprecision(2) << resultados[i] << endl;
	}
	

return 0;}
