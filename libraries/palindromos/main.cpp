#include <iostream>
#include <string>
#include <algorithm>

//librerias de C
#include <cstring>
#include <cstdio>
#include <ctype.h>

using namespace std;

typedef unsigned int uint ;
typedef char byte;

#include <vector>

string isPalindromo_dev(string t_palabra){//devolverá SI o NO como string

	string isPalindromo = "Si";
	for(uint i = 0, f = t_palabra.length()-1;
			i < t_palabra.length();
			i++, f--){
		const char &primeraLetra =	tolower(t_palabra[i]);
		const char &ultimaLetra = 	tolower(t_palabra[f]);

		if(primeraLetra != ultimaLetra){
			isPalindromo = "No";
		}
	}

	return isPalindromo;
}



void mainProgram(){
	vector<string> resultados;
	//pedir la cantidad de t_palabras
	int cantidadPalabras; cin >> cantidadPalabras;
	
	for(int i = 0; i < cantidadPalabras; i++){
		string palabra;
		//pedir la t_palabra
		fflush(stdin);
		getline(cin, palabra);

		palabra.erase(remove_if(palabra.begin(),
					palabra.end(),
					::isspace),
				palabra.end());
		//checar si esa t_palabra era palindromo o no
		resultados.push_back(isPalindromo_dev(palabra));
	}
	

	for(int i = 0; i < cantidadPalabras; i++){
		cout << resultados[i] << endl;
	}
}
int main(){
	mainProgram();

return 0;}
